package br.com.yuri.ioasys2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import br.com.yuri.ioasys2.Interface.IoasysServico;
import br.com.yuri.ioasys2.adapter.ListAdapter;
import br.com.yuri.ioasys2.models.Enterprise;
import br.com.yuri.ioasys2.models.EnterpriseSearchResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class HomeActivity extends AppCompatActivity {

    ImageView ic_search;
    Toolbar mToolbar_Search;
    Toolbar mToolbar_logo;
    RelativeLayout mRelativeLayout;
    TextView mTextView;
    SearchView search;

    Context context = this;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onResume() {
        super.onResume();
        if (getIntent().getBooleanExtra("EXIT", false)) {
            finish();
        }
        if(getIntent().getBooleanExtra("BACK", false)){
            mToolbar_logo.setVisibility(View.GONE);
            mTextView.setVisibility(View.GONE);
            mToolbar_Search.setVisibility(View.VISIBLE);
            mRelativeLayout.setVisibility(View.VISIBLE);

            mRecyclerView = (RecyclerView) findViewById(R.id.feed);
            mRecyclerView.setHasFixedSize(true);
            mLayoutManager = new LinearLayoutManager(HomeActivity.this);
            mRecyclerView.setLayoutManager(mLayoutManager);

            search.setQueryHint("Pesquisar");

            search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    BuscaAPI(query);
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    BuscaAPI(newText);
                    return true;
                }
            });
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mToolbar_Search = (Toolbar) findViewById(R.id.mToolbar_Search);
        mToolbar_logo = (Toolbar) findViewById(R.id.toolbarLogo);

        mRelativeLayout = (RelativeLayout) findViewById(R.id.mRecyclerView) ;

        mTextView = (TextView) findViewById(R.id.txt_cliquenabusca);
        search = (SearchView) findViewById(R.id.search);
        search.setIconifiedByDefault(false);

        ic_search = (ImageView) findViewById(R.id.ic_search);
        ic_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mToolbar_logo.setVisibility(View.GONE);
                mTextView.setVisibility(View.GONE);
                mToolbar_Search.setVisibility(View.VISIBLE);
                mRelativeLayout.setVisibility(View.VISIBLE);

                mRecyclerView = (RecyclerView) findViewById(R.id.feed);
                mRecyclerView.setHasFixedSize(true);
                mLayoutManager = new LinearLayoutManager(HomeActivity.this);
                mRecyclerView.setLayoutManager(mLayoutManager);

                search.setQueryHint("Pesquisar");

                search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        //Quando der submit
                        BuscaAPI(query);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        //Quando o texto mudar
                        BuscaAPI(newText);
                        return true;
                    }
                });



            }
        });



    }

    public void BuscaAPI(String name){
        SharedPreferences sharedPreferences = getSharedPreferences(IoasysServico.TOKEN_SHARED,0);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(IoasysServico.URL)
                .addConverterFactory(GsonConverterFactory.create()).build();

        IoasysServico servico = retrofit.create(IoasysServico.class);

        Call<EnterpriseSearchResponse> call = servico.getEnterprises(
                name,
                sharedPreferences.getString("access-token",""),
                sharedPreferences.getString("client",""),
                sharedPreferences.getString("uid",""));
        
        call.enqueue(new Callback<EnterpriseSearchResponse>() {
            @Override
            public void onResponse(Call<EnterpriseSearchResponse> call, Response<EnterpriseSearchResponse> response) {

                List<Enterprise> lista = response.body().getEnterprises();
                if(!lista.isEmpty()){
                    mTextView.setVisibility(View.GONE);
                    mAdapter = new ListAdapter(context, lista);
                    mRecyclerView.setAdapter(mAdapter);
                }
            }

            @Override
            public void onFailure(Call<EnterpriseSearchResponse> call, Throwable t) {
                Toast.makeText(HomeActivity.this, "Verifique sua conexão.", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent it = new Intent(getApplicationContext(), MainActivity.class);
        it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        it.putExtra("EXIT", true);
        startActivity(it);
    }
}
