package br.com.yuri.ioasys2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class DetailActivity extends AppCompatActivity {

    Toolbar toolbarDetail;
    ImageView imgDetailEnterprise;
    TextView enterpriseDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        toolbarDetail = (Toolbar) findViewById(R.id.toolbarDetail);
        imgDetailEnterprise = (ImageView) findViewById(R.id.imgDetailEnterprise);
        enterpriseDescription = (TextView) findViewById(R.id.enterpriseDescription);
        setSupportActionBar(toolbarDetail);

        Bundle extras = getIntent().getExtras();

        getSupportActionBar().setTitle(extras.getString("NameEnterprise"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbarDetail.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(getApplicationContext(), HomeActivity.class);
                it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                it.putExtra("BACK", true);
                startActivity(it);
            }
        });

        Glide.with(getApplication())
                .load(R.drawable.img_e_1_lista)
                .into(imgDetailEnterprise);

        enterpriseDescription.setText(extras.getString("DescriptionEnterprise"));





    }
}
