package br.com.yuri.ioasys2.Interface;

import br.com.yuri.ioasys2.models.EnterpriseSearchResponse;
import br.com.yuri.ioasys2.models.EnterpriseType;
import br.com.yuri.ioasys2.models.User;
import br.com.yuri.ioasys2.models.UserResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by yuri on 18/10/17.
 */

public interface IoasysServico {

    static String TOKEN_SHARED = "TokenSharedp";
    static String URL = "http://54.94.179.135:8090/api/v1/";


    @GET("enterprises/{id}")
    Call<EnterpriseType> getEnterpriseDetails(@Path("id") int id, @Header("access-token")String access_token,
                                              @Header("client")String client,
                                              @Header("uid")String uid);


    @GET("enterprises")
    Call<EnterpriseSearchResponse>getEnterprises(@Query("name") String name,
                                                 @Header("access-token")String access_token,
                                                 @Header("client")String client,
                                                 @Header("uid")String uid);

    @POST("users/auth/sign_in")
    Call<UserResponse> login(@Body User user);
}
