package br.com.yuri.ioasys2.models;

/**
 * Created by yuri on 18/10/17.
 */

public class TokenAccess {

    private String access_token;
    private String client;
    private String uid;

    public TokenAccess() {
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String acess_token) {
        this.access_token = acess_token;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
