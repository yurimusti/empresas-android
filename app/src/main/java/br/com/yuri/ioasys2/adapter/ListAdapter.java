package br.com.yuri.ioasys2.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import br.com.yuri.ioasys2.DetailActivity;
import br.com.yuri.ioasys2.HomeActivity;
import br.com.yuri.ioasys2.R;
import br.com.yuri.ioasys2.models.Enterprise;

/**
 * Created by yuri on 20/10/17.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.DataObjectHolder>{

    private List<Enterprise> mLista;
    private String NomeEmpresaViaUsuario;
    private Context context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder {
        //Postagem
         ImageView imageEnterprise;
         TextView nameEnterprise;
         TextView typeEnterprise;
         TextView countryEnterprise;
        CardView cardView;


        public DataObjectHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.cardview);
            imageEnterprise = (ImageView) itemView.findViewById(R.id.imageEnterprise);
            nameEnterprise = (TextView) itemView.findViewById(R.id.nameEnterprise);
            typeEnterprise = (TextView) itemView.findViewById(R.id.typeEnterprise);
            countryEnterprise = (TextView) itemView.findViewById(R.id.countryEnterprise);
        }

    }

//Construtor
    public ListAdapter(Context context, List<Enterprise> mLista) {
        this.context = context;
        this.mLista = mLista;

    }

    @Override
    public ListAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_itemlist, parent, false);
               return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {

        holder.nameEnterprise.setText(mLista.get(position).getEnterpriseName());
        holder.imageEnterprise.setImageResource(R.drawable.img_e_1_lista);
        holder.countryEnterprise.setText(mLista.get(position).getCountry());
        holder.typeEnterprise.setText(mLista.get(position).getEnterpriseType().getEnterprise_type_name());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent it = new Intent(context, DetailActivity.class);
                it.putExtra("NameEnterprise",mLista.get(position).getEnterpriseName());
                it.putExtra("DescriptionEnterprise",mLista.get(position).getDescription());
                context.startActivity(it);

            }
        });



    }

    @Override
    public int getItemCount() {
        return mLista.size();
    }
}
