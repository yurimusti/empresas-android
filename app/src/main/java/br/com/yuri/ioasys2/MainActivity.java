package br.com.yuri.ioasys2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import br.com.yuri.ioasys2.Interface.IoasysServico;
import br.com.yuri.ioasys2.models.User;
import br.com.yuri.ioasys2.models.UserResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private EditText etEmail;
    private EditText etPassword;
    private Button btnLogin;


    @Override
    protected void onResume() {
        super.onResume();
        if (getIntent().getBooleanExtra("EXIT", false)) {
            finish();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Context context = this;

        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);




            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(etEmail.getText().toString().isEmpty() || etPassword.getText().toString().isEmpty()){
                        if(etEmail.getText().toString().isEmpty()){
                            etEmail.setError("Por favor, digite um endereço de email.");
                        }
                        if(!etEmail.getText().toString().contains("@")){
                            etEmail.setError("Por favor, digite um endereço de email válido.");
                        }
                        if(etPassword.getText().toString().isEmpty()){
                            etPassword.setError("Por favor, digite a sua senha.");
                        }
                    }else{
                        User user = new User(etEmail.getText().toString(), etPassword.getText().toString());
                        Login(user, getApplicationContext());
                    }


                }
            });






    }

    public void Login(User user, Context context){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://54.94.179.135:8090/api/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        IoasysServico servico = retrofit.create(IoasysServico.class);

        Call<UserResponse> call = servico.login(user);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                
                if(response.isSuccessful()){
                    SharedPreferences sharedPreferences = getSharedPreferences("TokenSharedp",0);
                    SharedPreferences.Editor editor = sharedPreferences.edit();

                    editor.putString("access-token",response.headers().get("access-token"));
                    editor.putString("client",response.headers().get("client"));
                    editor.putString("uid",response.headers().get("uid"));
                    editor.commit();

                    Intent it = new Intent(MainActivity.this, HomeActivity.class);
                    it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(it);
                }else{
                    Toast.makeText(MainActivity.this, "Usuário não encontrado.", Toast.LENGTH_SHORT).show();
                }

                    
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Verifique sua conexão", Toast.LENGTH_SHORT).show();
            }
        });




    }


}
