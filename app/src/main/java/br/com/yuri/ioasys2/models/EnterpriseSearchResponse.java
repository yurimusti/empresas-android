package br.com.yuri.ioasys2.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by yuri on 20/10/17.
 */

public class EnterpriseSearchResponse {


    @SerializedName("enterprises")
    private List<Enterprise> enterprises;

    public EnterpriseSearchResponse(List<Enterprise> enterprises) {
        this.enterprises = enterprises;
    }

    public List<Enterprise> getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(List<Enterprise> enterprises) {
        this.enterprises = enterprises;
    }
}
